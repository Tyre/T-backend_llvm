(type i8 (i 8))
(type i16 (i 16))
(type i32 (i 32))
(type i64 (i 64))
(type i128 (i 128))

(type u8 (u 8))
(type u16 (u 16))
(type u32 (u 32))
(type u64 (u 64))
(type u128 (u 128))

(type f16 (f 16))
(type f32 (f 32))
(type f64 (f 64))
(type f80 (f 80))

