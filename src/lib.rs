use inkwell::{
    builder::Builder,
    context::Context,
    module::Module,
    support::load_library_permanently,
    targets::{CodeModel, FileType, InitializationConfig, RelocMode, Target, TargetMachine},
    types::{AnyTypeEnum, BasicType, BasicTypeEnum, StringRadix, VectorType},
    values::{
        ArrayValue, BasicValue, BasicValueEnum, FloatValue, FunctionValue, IntValue, PointerValue,
        StructValue, VectorValue,
    },
    AddressSpace, OptimizationLevel,
};
use std::path::Path;

use t_ree::{
    expressions::Expression,
    scope::{Definition, Scope},
    types::{FloatType, IntType, Primitive, Type},
};

use std::convert::TryInto;

pub struct ExtendedContext<'ctx> {
    context: &'ctx Context,
    module: Module<'ctx>,
    builder: Builder<'ctx>,
}

impl<'ctx> ExtendedContext<'ctx> {
    pub fn empty_tuple(&self) -> BasicValueEnum<'ctx> {
        BasicValueEnum::StructValue(self.context.struct_type(&[], true).const_named_struct(&[]))
    }
}

pub trait ScopeBackend {
    fn build(self, out: Option<String>);
}

impl ScopeBackend for Scope {
    fn build(self, out: Option<String>) {
        let context = Context::create();
        let context = ExtendedContext {
            context: &context,
            module: context.create_module("T"),
            builder: context.create_builder(),
        };

        for lib in self.libs {
            if load_library_permanently(&lib[..]) {
                panic!("Could not load library {}", lib)
            }
        }

        let mut function_definitions = Vec::new();
        for (key, value) in self.values {
            if let Definition::Function(_) | Definition::Extern(_, _, _) = value {
                let function = context.module.add_function(
                    &key[..],
                    value.typify().build(&context.context).try_into().unwrap(),
                    None,
                );
                function_definitions.push((function, value));
            } else {
                panic!("Global variables are not supported yet");
            }
        }
        for (function, value) in function_definitions {
            if let Definition::Function(expressions) = value {
                let entry = context.context.append_basic_block(function, "entry");
                context.builder.position_at_end(entry);
                context.builder.build_return(Some(&Expression::build_list(
                    expressions,
                    &context,
                    &function,
                )));
            }
        }
        context
            .module
            .verify()
            .expect("Module could not be verified");
        if let Some(out) = out {
            Target::initialize_native(&InitializationConfig::default())
                .expect("Native target could not be initialized");

            let opt = OptimizationLevel::Default;
            let reloc = RelocMode::Default;
            let model = CodeModel::Default;
            let triple = TargetMachine::get_default_triple();
            let target = Target::from_triple(&triple).expect("Default target not available");
            let target_machine = target
                .create_target_machine(&triple, "", "", opt, reloc, model)
                .unwrap();

            assert!(target_machine
                .write_to_file(&context.module, FileType::Object, &Path::new(&out))
                .is_ok());
        } else {
            let engine = context
                .module
                .create_execution_engine()
                .expect("Execution engine could not be created");
            unsafe {
                engine.run_function(
                    context
                        .module
                        .get_function("main")
                        .expect("Main function not defined"),
                    &[],
                );
            }
        }
    }
}

pub trait PrimitiveBackend {
    fn build<'ctx>(self, context: &'ctx Context) -> AnyTypeEnum<'ctx>;
}

impl PrimitiveBackend for Primitive {
    fn build<'ctx>(self, context: &'ctx Context) -> AnyTypeEnum<'ctx> {
        use FloatType::*;
        use IntType::*;
        use Primitive::*;
        match self {
            Bool => AnyTypeEnum::IntType(context.bool_type()),
            Int(ty) => AnyTypeEnum::IntType(match ty {
                I8 | U8 => context.i8_type(),
                I16 | U16 => context.i16_type(),
                I32 | U32 => context.i32_type(),
                I64 | U64 => context.i64_type(),
                I128 | U128 => context.i128_type(),
            }),
            Float(ty) => AnyTypeEnum::FloatType(match ty {
                //16 => context.f16_type(),
                F32 => context.f32_type(),
                F64 => context.f64_type(),
                //80 => context.x86_f80_type(),
                //128 => context.f128_type(),
            }),
        }
    }
}

pub trait TypeBackend {
    fn build<'ctx>(self, context: &'ctx Context) -> AnyTypeEnum<'ctx>;
}

impl TypeBackend for Type {
    fn build<'ctx>(self, context: &'ctx Context) -> AnyTypeEnum<'ctx> {
        use Type::*;
        match self {
            Primitive(p) => p.build(context),
            Tuple(types) => AnyTypeEnum::StructType(
                context.struct_type(
                    &types
                        .into_iter()
                        .map(|ty| {
                            ty.build(context)
                                .try_into()
                                .expect("Functions can not be stored in tuples")
                        })
                        .collect::<Vec<_>>()[..],
                    false,
                ),
            ),
            Array(count, ty) => AnyTypeEnum::ArrayType(
                TryInto::<BasicTypeEnum<'ctx>>::try_into(ty.build(context))
                    .expect("Arrays of non basic types are invalid")
                    .array_type(count as u32),
            ),
            Vector(count, ty) => AnyTypeEnum::VectorType(
                match TryInto::<BasicTypeEnum<'ctx>>::try_into(ty.build(context))
                    .expect("Vector of non basic types are invalid")
                {
                    BasicTypeEnum::IntType(ty) => ty.vec_type(count as u32),
                    BasicTypeEnum::FloatType(ty) => ty.vec_type(count as u32),
                    BasicTypeEnum::PointerType(ty) => ty.vec_type(count as u32),
                    _ => panic!("Vectors can only contain primitive types and pointers"),
                },
            ),
            Pointer(ty) => {
                use AnyTypeEnum::*;
                match ty.build(context) {
                    ArrayType(ty) => ty.ptr_type(AddressSpace::Generic),
                    FloatType(ty) => ty.ptr_type(AddressSpace::Generic),
                    FunctionType(ty) => ty.ptr_type(AddressSpace::Generic),
                    IntType(ty) => ty.ptr_type(AddressSpace::Generic),
                    PointerType(ty) => ty.ptr_type(AddressSpace::Generic),
                    StructType(ty) => ty.ptr_type(AddressSpace::Generic),
                    VectorType(ty) => ty.ptr_type(AddressSpace::Generic),
                    VoidType(_ty) => unreachable!(),
                }
                .into()
            }
            Function(ty, types, variadic) => AnyTypeEnum::FunctionType(
                TryInto::<BasicTypeEnum<'ctx>>::try_into(ty.build(context))
                    .expect("Function return type can not be a function")
                    .fn_type(
                        &types
                            .into_iter()
                            .map(|ty| {
                                TryInto::<BasicTypeEnum<'ctx>>::try_into(ty.build(context))
                                    .expect("Function argument type can not be a function")
                            })
                            .collect::<Vec<_>>()[..],
                        variadic.is_some(),
                    ),
            ),
        }
    }
}

pub trait ExpressionBackend: Sized {
    fn build_list<'ctx>(
        expressions: Vec<Self>,
        context: &ExtendedContext<'ctx>,
        fun: &FunctionValue<'ctx>,
    ) -> BasicValueEnum<'ctx>;

    fn build<'ctx>(
        self,
        context: &ExtendedContext<'ctx>,
        fun: &FunctionValue<'ctx>,
    ) -> BasicValueEnum<'ctx>;
}

impl ExpressionBackend for Expression {
    fn build_list<'ctx>(
        mut expressions: Vec<Expression>,
        context: &ExtendedContext<'ctx>,
        fun: &FunctionValue<'ctx>,
    ) -> BasicValueEnum<'ctx> {
        if let Some(expr) = expressions.pop() {
            for expr in expressions {
                expr.build(context, fun);
            }
            expr.build(context, fun)
        } else {
            Self::Tuple(Vec::new()).build(context, fun).into()
        }
    }

    fn build<'ctx>(
        self,
        context: &ExtendedContext<'ctx>,
        fun: &FunctionValue<'ctx>,
    ) -> BasicValueEnum<'ctx> {
        use Expression::*;
        let ty = self.typify();
        match self {
            Primitive(ty, value) => {
                let ty = ty.build(&context.context);
                use AnyTypeEnum::*;
                match ty {
                    IntType(ty) => ty
                        .const_int_from_string(&value[..], StringRadix::Decimal)
                        .expect("Invalid nuber representation")
                        .into(),
                    FloatType(ty) => ty.const_float_from_string(&value[..]).into(),
                    _ => unreachable!(),
                }
            }
            Tuple(values) => context
                .context
                .const_struct(
                    &values
                        .into_iter()
                        .map(|value| value.build(context, fun))
                        .collect::<Vec<_>>()[..],
                    true,
                )
                .into(),
            Array(values) => {
                if let Type::Array(_size, element_type) = ty {
                    use BasicTypeEnum::*;
                    match TryInto::<BasicTypeEnum>::try_into(element_type.build(context.context))
                        .unwrap()
                    {
                        IntType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                        ArrayType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                        FloatType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                        PointerType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                        StructType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                        VectorType(ty) => ty.const_array(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<_>>()[..],
                        ),
                    }
                    .into()
                } else {
                    unreachable!();
                }
            }
            Vector(values) => {
                if let Type::Vector(_size, element_type) = ty {
                    match TryInto::<BasicTypeEnum>::try_into(element_type.build(context.context))
                        .unwrap()
                    {
                        BasicTypeEnum::IntType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<IntValue<'ctx>>>()[..],
                        ),
                        BasicTypeEnum::ArrayType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<ArrayValue<'ctx>>>()[..],
                        ),
                        BasicTypeEnum::FloatType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<FloatValue>>()[..],
                        ),
                        BasicTypeEnum::PointerType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<PointerValue<'ctx>>>()[..],
                        ),
                        BasicTypeEnum::StructType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<StructValue<'ctx>>>()[..],
                        ),
                        BasicTypeEnum::VectorType(_ty) => VectorType::const_vector(
                            &values
                                .into_iter()
                                .map(|value| value.build(context, fun).try_into().unwrap())
                                .collect::<Vec<VectorValue<'ctx>>>()[..],
                        ),
                    }
                    .into()
                } else {
                    unreachable!();
                }
            }
            Pointer(value) => {
                let memory = context.builder.build_alloca(
                    TryInto::<BasicTypeEnum<'ctx>>::try_into(value.typify().build(context.context))
                        .expect("Pointer to non basic value not allowed yet"),
                    "alloca",
                );
                context
                    .builder
                    .build_store(memory, value.build(context, fun));
                memory.into()
            }

            Call(val, args) => {
                let args = args
                    .into_iter()
                    .map(|arg| arg.build(context, fun).as_basic_value_enum())
                    .collect::<Vec<_>>();
                match *val {
                    Global(_ty, name) => context.builder.build_call(
                        context
                            .module
                            .get_function(&name[..])
                            .expect(&format!("`{}` is not a function", name)[..]),
                        &args[..],
                        &name[..],
                    ),
                    _ => unimplemented!(),
                }
                .try_as_basic_value()
                .left()
                .unwrap_or(context.empty_tuple())
            }
            Block(expressions) => Expression::build_list(expressions, context, fun),
            Bitcast(val, ty) => context.builder.build_bitcast(
                val.build(context, fun),
                TryInto::<BasicTypeEnum<'ctx>>::try_into(ty.build(context.context))
                    .expect("Transmute to non basic values not possible"),
                "bitcast",
            ),
            Store(ptr, val) => {
                context.builder.build_store(
                    ptr.build(context, fun).into_pointer_value(),
                    val.build(context, fun).into_pointer_value(),
                );
                context.empty_tuple()
            }
            Load(ptr) => context
                .builder
                .build_load(ptr.build(context, fun).into_pointer_value(), "load"),
            Global(_, _) => unimplemented!(),
            Argument(_, index) => fun.get_nth_param(index).expect("Argument does not exist"),
        }
    }
}
